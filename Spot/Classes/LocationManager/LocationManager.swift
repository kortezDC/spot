//  LocationManager.swift
//  Spot

import Foundation
import CoreLocation
import GooglePlaces

class LocationManager : NSObject {
    
    var locationManager : CLLocationManager? = nil
    var placesClient : GMSPlacesClient? = nil

    internal var place : String? = nil
    internal var address : String? = nil
    internal var city : String? = nil
    internal var currentLocation : CLLocation? = nil
    
    var currentPlace : String {
        get{
            guard let detectedPlace = place else {
                return "No current place"
            }
            return detectedPlace
        }
    }
    
    var currentAddress : String {
        get{
            guard let detectedAddress = address else {
                return "No current address"
            }
            return detectedAddress
        }
    }

    var currentCity : String {
        get{
            guard let detectedCity = city else {
                return "No current city"
            }
            return detectedCity
        }
    }
    
    var getAuthorizationStatus : CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }

    override init() {
        super.init()
        
        locationManager = CLLocationManager()
        getCurrentLocation()
        GMSPlacesClient.provideAPIKey(ProjectDefaults.getPlistValue(valueFor: PlistKeys.Places))
        GMSPlacesClient.openSourceLicenseInfo()
        placesClient = GMSPlacesClient()
        
        /// Register for the applicationDidBecomeActive anywhere in your app.
        NotificationCenter.default.addObserver(self, selector: #selector(getCurrentPlace), name: NSNotification.Name.UIApplicationDidBecomeActive, object: UIApplication.shared)        
    }
    
    func checklLocationAuthorizationStatus(status: CLAuthorizationStatus){
        switch status {
        case .notDetermined:
            locationManager!.requestWhenInUseAuthorization()
        case .denied:
            UIApplication.showAlertMessage(message: "Location access is off. 'SPotBoss' is unable to access your  location and you won't create or get info of the nearby spots. We recommend to turn it on. For this, please go to Settings and allow access to your location.")
        default:
            getCurrentLocation()
        }
     }
    
    /// Singleton
    static let shared: LocationManager = {
        let instance = LocationManager()
        return instance
    }()
    
    func getCurrentLocation(){
        
        // Set the delegate
        locationManager!.delegate = self
        locationManager!.startUpdatingLocation()
    }

    //    func getCurrentPlace(complete: @escaping () -> ()) {
    /// Get current palce
    @objc func getCurrentPlace() {
        
        placesClient?.currentPlace { (placeList, error) in
            if let error = error {
                print("[Debug info] Place error: \(error.localizedDescription)")
                return
            }
            
            if let places = placeList {
                let place = places.likelihoods.first?.place
                if let place = place {
                    
                    self.place = place.name
                    /// Get user's current location name
                    let geocoder = CLGeocoder()
                    self.currentLocation = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                    geocoder.reverseGeocodeLocation(self.currentLocation!) { (placemarksArray, error) in
                        
                        if (placemarksArray?.count)! > 0 {
                            let placemark = placemarksArray?.first
                            /// City
                            if let city = placemark?.addressDictionary?["City"] as? String {
                                self.city = city
                            }
                            /// Street address
                            if let street = placemark?.addressDictionary?["Thoroughfare"] as? String {
                                self.address = street
                            }
                        }
                    }
                }
            }
        }
    }
}

extension LocationManager : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        /// Remove delegate for the only the first occurrence
        manager.delegate = nil
        if (locations.first?.coordinate.latitude) != nil {
            print("[Debug info] - Location updated")
            /// Save geolocation data
            currentLocation = (locations.first)!
            manager.stopUpdatingLocation()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LocationUpdated"), object: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checklLocationAuthorizationStatus(status: status)
    }
}
