//  UserManager.swift
//  Spot

import Foundation
import Firebase

class UserManager {
    
    /// Singleton
    static let shared: UserManager = {
        
        let instance = UserManager()
        return instance
    }()
    
    var getLoggedInUser : FIRUser? {
    
        guard let alreadySignedIn = FIRAuth.auth()?.currentUser else{
            return nil
         }
        return alreadySignedIn
    }    
}
