//  BaseController.swift
//  Spot

import UIKit

class BaseController: UIViewController {
    /// Keyboard protocol
    var keyboardYView : CGFloat = 0.0
    var keyboardHeightView : CGFloat = 0.0
    var keyboardFullKeyboard: Bool = false
    var keyboardDeltaHeightKeyboard: CGFloat = 0.0
    var addGesture: Bool = true
    lazy var tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.gestureTapOnView))
    var isShowKeyboard: Bool = false
    /// router
    lazy var router: RouterModel! = RouterModel(presenter: self.navigationController)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.async {
            self.keyboardYView = self.view.frame.origin.y
            self.keyboardHeightView = self.view.frame.size.height
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Actions
    func actionBackBarItem() {
        router.popController()
    }
}
