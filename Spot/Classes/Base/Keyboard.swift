//  Keyboard.swift
//  Spot

import UIKit

protocol Keyboard: class, NSObjectProtocol {
    
    /// keyboard
    var keyboardYView : CGFloat {set get}
    var keyboardHeightView : CGFloat {set get}
    var keyboardFullKeyboard: Bool {set get}
    var keyboardDeltaHeightKeyboard: CGFloat {set get}
    /// gesture
    var tapGesture : UITapGestureRecognizer {set get}
    var addGesture: Bool {get set}
    /// show(status)
    var isShowKeyboard: Bool {get set}
    /// func
    func showKeyboardUpdateView(_ aNotification:Notification)
    func hideKeyboardUpdateView(_ aNotification:Notification)
    func showKeyboardUpView(_ aNotification:Notification)
    func hideKeyboardUpView(_ aNotification:Notification)
}
