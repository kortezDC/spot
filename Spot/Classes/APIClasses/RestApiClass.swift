//
//  RestApiClass.swift
//  GamblingGame
//
//  Created by Kortez  on 13.07.16.
//  Copyright © 2016 zen-tech. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class RestApiClass {
    
    // MARK: - Singleton
    static let sharedInstance : RestApiClass = {
        let instance = RestApiClass()
        return instance
    }()
    
    let reachabilityManager = NetworkReachabilityManager(host: "www.apple.com")
    
    /**
     *  Start network listener
     */
    func createNetworkListener(){
        reachabilityManager?.listener = { status in
            
            switch status {
                
            case .notReachable:
                print("network connection status - lost")
            case .reachable(NetworkReachabilityManager.ConnectionType.ethernetOrWiFi):
                print("network connection status - ethernet/WiFI")
            case .reachable(NetworkReachabilityManager.ConnectionType.wwan):
                print("network connection status - wwan")
            default:
                break
            }
            /*
             TODO - code for procced outstanding request
             */
        }
        reachabilityManager?.startListening()
    }
    
    /**
     *  Cancel only task with ID
     */
    func cancelRequestByID(_ ind : Int) {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach {
                
                if $0.taskDescription == String(ind) {
                    print("task canceled")
                    $0.cancel()
                }
            }
        }
    }

    /**
     *  Cancel all task
     */
    func cancelAllRequests() {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (dataTasks, uploadTasks, downloadTasks) in
            dataTasks.forEach { $0.cancel() }
        }
    }

    static func callAPIResponse <T:Mappable> (method: HTTPMethod, type:T.Type, params:Dictionary<String, Any>?, headers: Dictionary<String, String>?, success:@escaping (T)->Void)->Void where T:Mappable,T:Meta {
        
        let fullPath = checkFullUrlPath(currentPath: type.url_get(method: method.rawValue), params: params)
        print(fullPath)
        Alamofire.request(fullPath, method:method, parameters: params, headers:headers).responseObject(queue: nil, keyPath: nil, context: nil) { (response: DataResponse<T>) in
            
            proccedResponse(response: response, success: { (result) in
                success(result)
            })
        }
    }
    
    /**
     * Functions for parse response:
     */
    static func proccedResponse <T:Mappable> (response:DataResponse<T>, success:@escaping (T)->Void) {
        
        let data = response.data
        print(String(data: data!, encoding: String.Encoding.utf8) ?? "")
        
        switch response.result {
            
        case .success (let item):
            if let paginationTotalCount = response.response?.allHeaderFields["X-Pagination-Total-Count"] as? String, let totalCount = Int(paginationTotalCount), item is ResponseSimpleArrayModel {
                (item as? ResponseSimpleArrayModel)?.totalCount = totalCount
            }
            
            success(item)
            
        case .failure(let error as NSError):
            
            switch error.localizedDescription {
            case "cancelled":
                print("response canceled")
                break;
            default:
                UIApplication.showAlertMessage(message: error.localizedDescription)
            }

        default:break
        }
    }
    
    /**
     * Functions for get additional url path params:
     */
    static func checkFullUrlPath(currentPath: String, params:Dictionary<String, Any>?) -> String {
        
        guard params?["additionalUrlPath"] != nil else {
            return currentPath
        }
        return currentPath + "/" + (params!["additionalUrlPath"] as! String)
    }
}
