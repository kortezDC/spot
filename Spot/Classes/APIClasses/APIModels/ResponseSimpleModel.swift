//
//  LoginModel.swift
//  GamblingGame
//
//  Created by Kortez  on 08.08.16.
//  Copyright © 2016 gbksoft. All rights reserved.
//
import Foundation
import ObjectMapper

class ResponseSimpleModel : Mappable {
    
    var status = ""
    var code = 0
    var message = ""
    
    // Implementation of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        code <- map["code"]
        status <- map["status"]
//        result <- map["result"]
        message <- map["message"]
    }
    
    class func header() -> [String:String] {
        return ["Authorization": "Bearer ",
                "device_id": ""]
    }    
}

class ResponseSimpleArrayModel : Mappable {
    
    var status = ""
    var code = 0
    var message = ""
    var totalCount: Int?
    
    // Implementation of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        code <- map["code"]
        status <- map["status"]
        message <- map["message"]
    }
    
    static func header() -> [String:String] {
        
        return ["Authorization": "Bearer ",
            "device_id": ""]
    }
}
