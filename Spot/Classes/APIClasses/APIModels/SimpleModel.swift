//
//  SimpleModel.swift
//  FirebaseTest
//
//  Created by Kortez on 09.02.17.
//  Copyright © 2017 ZenTech. All rights reserved.
//

import Foundation
import ObjectMapper

class SimpleModel : ResponseSimpleModel, Meta {
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        
        code <- map["code"]
        status <- map["status"]
        message <- map["message"]
    }
    
    // Implementation of Meta protocol
    static func url_get(method: String) -> String {
        
        return "empty"
    }

}
