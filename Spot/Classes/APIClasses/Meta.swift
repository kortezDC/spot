//
//  Meta.swift
//  GamblingGame
//
//  Created by mac14  on 26.10.16.
//  Copyright © 2016 GBKSoft. All rights reserved.
//

import Foundation

protocol Meta {
    
    static func url_get(method:String)->String
}
