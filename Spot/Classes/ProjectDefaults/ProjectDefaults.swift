//  ProjectDefaults.swift
//  PromiseExample

import Foundation

enum PlistKeys : String {
    
    case Maps   = "GoogleMapsAPIKey"
    case Places = "GooglePlacesAPIKey"
    case FIRStorageURL
}

enum Firebase {
    case FirebaseStorage
}

/// For debuging
enum DefaultGeo : Double {
    
    case latitude = 42.152981
    case longitude = -80.131197
}

/**
 * Project defaults and constants credentials
 */

class ProjectDefaults {
    
    /// GoogleMaps/Places credentials
    class func getPlistValue(valueFor key : PlistKeys)->String {
        if let val = Bundle.main.object(forInfoDictionaryKey: key.rawValue) as? String {
            return val
        }else{
            return "parameter: \(key.rawValue) - not set in info.plist"
        }
    }    
}
