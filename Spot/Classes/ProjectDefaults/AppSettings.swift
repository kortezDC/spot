//  AppSettings.swift
//  PresqueIslePark

import Foundation


struct AppSeetings {
    
    let userDefault = UserDefaults.standard
    let userDefaultAccessToken = "UserDefaultAccessToken"
    let userDefaultAccessTokenExpiredAt = "UserDefaultAccessTokenExpiredAt"
    let userDefaultAccessDeviceToken = "UserDefaultAccessDeviceToken"
    
    var pageAllCount = 0
    var pageCurrent = 1
    var perPage = 50
    
    // MARK: - Singleton
    static var sharedInstance : AppSeetings = {
        let instance = AppSeetings()
        return instance
    }()
    
    func recordDeviceToken(token: String) {
       
        userDefault.set(token, forKey: userDefaultAccessDeviceToken)
        userDefault.synchronize()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DeviceTokenUpdated"), object: nil)
    }
    
    func getDeviceToken() -> String? {
        
        if let token = userDefault.object(forKey: userDefaultAccessDeviceToken){
            return token as? String
        }else{
            // for simulator debug
            // "5e334cc19489d7016ff8d420bf3a60138044ae6fba0bea10040b82f986d541dc"
            return nil
        }
    }
    
    func recordAccessToken(token: String, expiredAt: Int) -> Bool {
        
        userDefault.set(token, forKey: userDefaultAccessToken)
        userDefault.set(expiredAt, forKey: userDefaultAccessTokenExpiredAt)
        return userDefault.synchronize()
    }
    
    func recordAccessTokenExpiredAt(expiredAt: Int) {
        userDefault.set(expiredAt, forKey: userDefaultAccessTokenExpiredAt)
        userDefault.synchronize()
    }
    
    func getAccessToken() -> (token: String?, expiredAt: Int)? {
        if let token = userDefault.object(forKey: userDefaultAccessToken) as? String {
            return (token, userDefault.integer(forKey: userDefaultAccessTokenExpiredAt))
        } else {
            return nil
        }
    }
    
    func clearAccessToken() {

        userDefault.set(nil, forKey: userDefaultAccessToken)
        userDefault.set(0, forKey: userDefaultAccessTokenExpiredAt)
        userDefault.synchronize()
    }
    
    mutating func recordPaginationValues(current: Int, count: Int){
        pageCurrent = current
        pageAllCount = count
    }
    
    mutating func recordDefaultPaginationValues(){
        pageCurrent = 0
        pageAllCount = 1
    }
}
