//  SpotModel.swift
//  Spot

import Foundation
import ObjectMapper
import CoreLocation

class SpotModel: BaseObjectModel {
    
    var spotUID = ""
    var ownerUID = ""
    var ownerName = ""
    var title = ""
    var spotDescription = ""
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var radius = 0.1 // value in km
    var images = [ImageModel]()
    var previews = 0
    var likes = 0
    
    // Mappable
    override func mapping(map: Map) {
        spotUID  <- map["spotUID"]
        ownerUID  <- map["uid"]
        ownerName <- map["ownerName"]
        title <- map["title"]
        spotDescription  <- map["description"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        radius <- map["radius"]
        timestamp <- map["timestamp"]
        previews <- map["previews"]
        likes <- map["likes"]

        var myimages : [String:Any]? = nil
        myimages <- map["images"]
        
        if myimages != nil {
            for image in (myimages?.values)! {
                if let imageParams = image as? [String:Any] {
                    images.append(ImageModel(JSON: imageParams)!)
                }
            }
        }
    }
    
    var spotCoordinates : CLLocationCoordinate2D {
        return  CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
