//  ImageModel.swift
//  Spot

import Foundation
import ObjectMapper

class ImageModel : BaseObjectModel {
    
    var entityUID = ""
    var ownerUID = ""
    var imageUID = ""
    var ownerName = ""
    var title = ""
    var spotUID = ""
    var imageURL = ""
    var previews = 0
    var likes = 0
    var latitude : Double = 0.0
    var longitude : Double = 0.0

    // Mappable
    override func mapping(map: Map) {
        entityUID  <- map["entityUID"]
        ownerUID  <- map["ownerUID"]
        imageUID  <- map["imageUID"]
        ownerName <- map["ownerName"]
        title <- map["title"]
        spotUID  <- map["spotUID"]
        imageURL <- map["imageURL"]
        timestamp <- map["timestamp"]
        previews <- map["previews"]
        likes <- map["likes"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
     }
}
