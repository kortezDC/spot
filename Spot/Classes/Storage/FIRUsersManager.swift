//  FIRUsers.swift
//  Spot

import Foundation
import Firebase

protocol FIRUserManagerProtocol {
    
    func createUserModelFIB(user: UserModel)
    func getUserByUID(userUID: String, success:@escaping (UserModel)->())
}

extension FIRUserManagerProtocol {
    
    /// Following method is a add user's  more details
    func createUserModelFIB(user: UserModel){
        RootRouter.shared.firebaseRef.child(FIRChild.Users.rawValue).child(user.uid).setValue(user.toJSON())
    }

    /// Following method get the registers user details
    func getUserByUID(userUID: String, success:@escaping (UserModel)->()) {
        
        RootRouter.shared.firebaseRef.child(FIRChild.Users.rawValue).child(userUID).observeSingleEvent(of: .value, with: { snapshot in
            let postDict = snapshot.value as! [String : Any]
            success(UserModel(JSON: postDict)!)
        })
    }
}
