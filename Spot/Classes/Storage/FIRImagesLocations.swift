//  FIRImagesLocations.swift
//  Spot

import Foundation
import GeoFire
import Firebase

protocol FIRImagesLocationsProtocol : FIRBaseProtocol{
    
    func getFIBUserCreatedImages()
    func getFIBImage(imageSpotKey: String, complete: @escaping (ImageModel) ->())
    func getImagesByLocation(spot: SpotModel)    
}

extension FIRImagesLocationsProtocol  where Self: ViewModel {
    
    /// Get all images for current user
    ///
    /// - Parameters:
    ///   - model: represents controller model, who controll and procced spots
    ///   - complete: succes block
    func getFIBUserCreatedImages(){
        
        self.container = []
        let query = firebaseRef.queryOrdered(byChild: "ownerUID").queryEqual(toValue: UserManager.shared.getLoggedInUser?.uid)
        query.observeSingleEvent(of: .value, with: { snapshot in
            if snapshot.value is NSNull {
                print("[Debug info] - User images empty")
            } else {
                let images = snapshot.value as? [String: Any]
                for child in images! {
                    if let dictData = child.value as? [String : Any] {
                        self.container.append(ImageModel(JSON: dictData)!)
                    }
                }
                self.delegate?.viewModelDidEndUpdate()
            }
        })
    }
    
    func getFIBImage(imageSpotKey: String, complete: @escaping (ImageModel) ->()){
        
        firebaseRef.child(imageSpotKey).observeSingleEvent(of: .value, with: { (responseData) in
            if let dictData = responseData.value as? [String : AnyObject] {
                complete(ImageModel(JSON: dictData)!)
            }
        })
    }
    
    /// Find images by spots location
    ///
    /// - Parameter model: represents controller model, who controll and procced founded spots
    func getImagesByLocation(spot: SpotModel) {
        
        self.container = []
        /// Populate list of keys
        var locationsKeys = [String?]()
        
        UIApplication.showHUD()
        let spotLocation = CLLocation(latitude: spot.latitude, longitude: spot.longitude)
        let spotsInLocations = geoFire?.query(at: spotLocation, withRadius: spot.radius)
        
        /// Observe list of locations
        spotsInLocations!.observe(.keyEntered, with: { (key, location) in
            locationsKeys.append(key)
        })
        
        /// Do something with list of keys.
        spotsInLocations!.observeReady({
            if locationsKeys.count > 0 {
                for key in locationsKeys {
                    DispatchQueue.main.async {
                        self.getFIBImage(imageSpotKey: key!, complete: { (imageModel) in
                            self.container.append(imageModel)
                            if self.container.count == locationsKeys.count {
                                self.delegate?.viewModelDidEndUpdate()
                            }
                        })
                    }
                }
            }else{
                /// Not found new spots in that radius
                UIApplication.hideActivity()
            }
        })
    }
}
