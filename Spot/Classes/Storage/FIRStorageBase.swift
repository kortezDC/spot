//  FIRStorage.swift
//  Spot

import UIKit
import Firebase
import FirebaseStorage

enum FIRChild: String {
    case SpotsModels
    case ImagesModels
    case GeofireSpots
    case GeofireImages
    case Users
    case PreviewModels
    
    var getParamName : String {
    
        switch self{
        case .PreviewModels:
            return ChildParams.previews.rawValue
        default:
            return ChildParams.likes.rawValue
        }
    }
}

enum ChildParams : String {
    
    case previews
    case likes
}

enum FIRStorageError: Error {
    case uploadSpotPhoto
    case uploadProfilePhoto
}

class FIRStorageBase: NSObject {
    
    /// firebase storage
    let storage = FIRStorage.storage()
    /// storafe referece with url
    let storageRef = FIRStorage.storage().reference(forURL: ProjectDefaults.getPlistValue(valueFor: PlistKeys(rawValue: PlistKeys.FIRStorageURL.rawValue)!))
    
    let spot = "Spot"
    /// photo
    let entryName = "photo"
    
    /// profile
    let profileName = "Profile"
    /// spot images
    let SpotImages = "SpotImages"
    
    func getSpotEntryRef() -> FIRStorageReference? {
        guard let uid = FIRAuth.auth()?.currentUser?.uid else { return nil}
        let name = "photo_spot.jpg"
        return storageRef.child("\(spot)/\(entryName)/\(uid)/\(name)")
    }
    
    func getRefForSpot() -> FIRStorageReference? {
        
        guard let uid = FIRAuth.auth()?.currentUser?.uid else { return nil}
        let imageName = Helper.stringWithLength()
        let name = "\(imageName)spot_image.jpg"
        return storageRef.child("\(spot)/\(SpotImages)/\(uid)/\(name)")
    }
    
    /// for future functionality
    func getProfileRef() -> FIRStorageReference? {
        guard let uid = FIRAuth.auth()?.currentUser?.uid else { return nil}
        let name = "user_profile.jpg"
        return storageRef.child("\(spot)/\(profileName)/\(uid)/\(name)")
    }
}

/// MARK: delete, for future functionality
extension FIRStorageBase {
    
    func deleteFile(gs: String , complete: ((Error?) -> ())?) {
        // Create a reference to the file to delete
        let _ref = self.storageRef.child(gs)
        // Delete the file
        _ref.delete { (error) -> Void in
            complete?(error)
        }
    }
}
