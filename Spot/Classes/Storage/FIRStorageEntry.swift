//  FIRStorageEntry.swift
//  Spot

import UIKit
import Firebase

class FIRStorageEntry: FIRStorageBase{

    /// Upload
    func uploadPhoto(_ image: UIImage, complate: @escaping (URL?, String?) -> (), error: @escaping (Error) ->()) {
        
        guard let ref = self.getSpotEntryRef() else {
            error(FIRStorageError.uploadSpotPhoto)
            return
        }

        /// Resize image
        ref.put(UIImageJPEGRepresentation(image, 0.8)!, metadata: nil) { (metadata, err) in
            
            if let _ = err {
                error(FIRStorageError.uploadSpotPhoto)
            }
            complate(metadata?.downloadURL(), ref.fullPath)
        }
    }
 }
