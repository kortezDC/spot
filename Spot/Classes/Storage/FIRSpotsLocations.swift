//  FIRSpotsLocations.swift
//  Spot

import Foundation
import Firebase
import CoreLocation
import GeoFire
import GoogleMaps

protocol FIRSpotsLocationsProtocol : FIRBaseProtocol {
    
    func getSpotsNearBy()
    func getSpotsByFilter(childParams: ChildParams)
    func getFIBSpots(locationKey : String, complete: @escaping (SpotModel) ->())
    func getFIBUserSpots()
    func fillMapsView(map:GMSMapView)
}

extension FIRSpotsLocationsProtocol where Self : ViewModel{
    
    /// Find near by spots by current location
    func getSpotsNearBy() {
        
        self.container = []
        var spotLocation = LocationManager.shared.currentLocation
        /// Populate list of keys
        var locationsKeys = [String?]()
        
        /// Check if current user already have the spot in specified location
        if let latitude = spotLocation?.coordinate.latitude, let longitude = spotLocation?.coordinate.longitude {
            
            UIApplication.showHUD()
            spotLocation = CLLocation(latitude: latitude, longitude: longitude)
            let spotsInLocations = geoFire?.query(at: spotLocation, withRadius: 0.1)
            
            /// Observe list of locations
            spotsInLocations!.observe(.keyEntered, with: { (key, location) in
                locationsKeys.append(key)
            })
            
            /// Do something with list of keys.
            spotsInLocations!.observeReady({
                print("[Debug info] spots locations count = \(locationsKeys.count)")
                if locationsKeys.count > 0 {
                    for key in locationsKeys {
                        DispatchQueue.main.async {
                            self.getFIBSpots(locationKey: key!, complete: { (spot) in
                                self.container.append(spot)
                                if self.container.count == locationsKeys.count {
                                    self.delegate?.viewModelDidEndUpdate()
                                }
                            })
                        }
                    }
                }else{
                    /// Not found new spots in that radius
                    UIApplication.hideActivity()
                }
            })
        }
    }
    
    /// Find most recent spots
    ///
    /// - Parameter childParams:
    func getSpotsByFilter(childParams: ChildParams){
        
        self.container = []
        let query = firebaseRef.queryOrdered(byChild: childParams.rawValue).queryLimited(toLast: queryFilterLimit)
        query.observeSingleEvent(of: .value, with: { snapshot in
            if snapshot.value is NSNull {
                print("[Debug info] - spotsmodels empty")
            } else {
                let spots = snapshot.value as? [String: Any]
                for child in spots! {
                    if let dictData = child.value as? [String : Any], let spot = SpotModel(JSON: dictData) {
                        spot.spotUID = child.key
                        self.container.append(spot)
                    }
                }
                self.delegate?.viewModelDidEndUpdate()
            }
        })
    }
    
    /// Get FIB SpotModel
    ///
    /// - Parameters:
    ///   - locationKey: by location key value
    ///   - complete: return founded model
    func getFIBSpots(locationKey : String, complete: @escaping (SpotModel) ->()){
        firebaseRef.child(locationKey).observeSingleEvent(of: .value, with: { (responseData) in
            
            if var dictData = responseData.value as? [String : AnyObject] {
                dictData.updateValue(responseData.key as AnyObject, forKey: "spotUID")
                complete(SpotModel(JSON: dictData)!)
            }
        })
    }
    
    /// Get All Spots for current user
    ///
    /// - Parameters:
    ///   - model: represents controller model, who controll and procced spots
    ///   - complete: succes block
    func getFIBUserSpots(){
        let query = firebaseRef.queryOrdered(byChild: "uid").queryEqual(toValue: UserManager.shared.getLoggedInUser?.uid)
        query.observeSingleEvent(of: .value, with: { snapshot in
            if snapshot.value is NSNull {
                print("[Debug info] - User spotsmodels empty")
            } else {
                let spots = snapshot.value as? [String: Any]
                for child in spots! {
                    if let dictData = child.value as? [String : Any] {
                        self.container.append(SpotModel(JSON: dictData)!)
                    }
                }
                self.delegate?.viewModelDidEndUpdate()
            }
        })
    }
    
    /// Add spot markers to controller map
    func fillMapsView(map:GMSMapView) {
        
        // remove all markers from map
        map.clear()
        // creare area with markers
        let path = GMSMutablePath()
        
        if self.container is [SpotModel] {
            for spot in self.container as! [SpotModel]{
                
                // Creates a marker
                let spotMarker = GMSMarker(position: spot.spotCoordinates)
                //            buoyMarker.icon = UIImage(named: "map_pin")
                spotMarker.title = spot.title
                spotMarker.appearAnimation = GMSMarkerAnimation.none
                spotMarker.map = map
                spotMarker.userData = spot
                path.add(spot.spotCoordinates)
            }
            let bounds = GMSCoordinateBounds(path: path)
            // Set camera square position
            map.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50))
        }
    }
}
