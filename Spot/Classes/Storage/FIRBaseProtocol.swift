//  FIRBaseProtocol.swift
//  Spot
//
//  Created by Kortez on 09.03.17.

import Foundation
import Firebase
import GeoFire

protocol FIRBaseProtocol {
    
    var firebaseRef : FIRDatabaseReference { get set }
    var geoFire : GeoFire? { get set}
    var queryFilterLimit : UInt {get set}
}
