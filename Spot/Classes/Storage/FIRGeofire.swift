//  FIRGeofire.swift
//  Spot

import Foundation
import GeoFire
import CoreLocation

protocol FIRGeofireProtocol {
    
    func addGeofire(child:FIRChild, nodeKey: String ,locations:CLLocation)
}

extension FIRGeofireProtocol {
    
    /// Create Geofire for child model
    ///
    /// - Parameters:
    func addGeofire(child:FIRChild, nodeKey: String ,locations:CLLocation) {
        let geoFire: GeoFire = GeoFire(firebaseRef: RootRouter.shared.firebaseRef.child(child.rawValue))
        geoFire.setLocation(locations, forKey: nodeKey)
    }
}
