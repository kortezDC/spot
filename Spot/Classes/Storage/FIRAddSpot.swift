//  FIRAddSpot.swift
//  Spot

import Foundation
import Firebase
import CoreLocation
import GeoFire

protocol FIRAddSpotProtocol : FIRBaseProtocol, FIRGeofireProtocol {
    
    func proccedSpot(title:String?, description:String?)
    func addSpot(location spotLocation : CLLocation)
    func alertAction(alertAction : UIAlertAction)
    
    var newSpot : SpotModel? {get set}
}

extension FIRAddSpotProtocol {
    
    func proccedSpot(title:String? = "", description:String? = "") {
        
        if newSpot == nil {
            UIApplication.showAlertMessage(message: "Spot must have location, please select location on the map")
            return
        }
        
        var spotLocation = CLLocation()
        
        if (title?.characters.count)! < 1 {
            newSpot?.title = LocationManager.shared.currentCity
        }else{
            newSpot?.title = title!
        }
        
        newSpot?.spotDescription = description!
        
        /// Check if current user already have the spot in specified location
        if let latitude = newSpot?.latitude, let longitude = newSpot?.longitude {
            
            spotLocation = CLLocation(latitude: latitude, longitude: longitude)
            addSpot(location: spotLocation)
        }
    }
    
    func addSpot(location spotLocation : CLLocation){
        
        let newSpotRef = firebaseRef.child(FIRChild.SpotsModels.rawValue).childByAutoId()
        let currentUser = UserManager.shared.getLoggedInUser
        newSpot!.ownerUID = currentUser!.uid
        newSpot!.ownerName = currentUser!.displayName!
        newSpot?.timestamp = Date().timeIntervalSince1970
        
        /// Firebase add new spot to user
        newSpotRef.setValue(newSpot?.toJSON()) { (error, reference) in
            // Add location for new spot
            self.addGeofire(child: .GeofireSpots, nodeKey: reference.key, locations: spotLocation)
            
            if error != nil {
                UIApplication.showAlertMessage(message: (error?.localizedDescription)!)
            }else{
                UIApplication.showAlertMessageWithAction(message: "Spot successfully added", handler: self.alertAction)
            }
        }
    }
}
