//  FIRStoragePhoto.swift
//  Spot

import UIKit
import Firebase

class FIRStorageSpotPhoto: FIRStorageBase {

    /// MARK: Upload
    func uploadPhoto(_ image: UIImage, complate: @escaping (URL?, String?) -> (), error: @escaping (Error) ->()) {
        
        guard let ref = self.getRefForSpot() else {
            error(FIRStorageError.uploadSpotPhoto)
            return
        }
        
        ref.put(UIImageJPEGRepresentation(image, 1)!, metadata: nil) { (metadata, err) in
            if let _ = err {
                error(FIRStorageError.uploadSpotPhoto)
            }
            complate(metadata?.downloadURL(), ref.fullPath)
        }
    }
    
    func getImage(imageUIDURL: String, complete: @escaping (UIImage) ->()){
        
        storage.reference(forURL: imageUIDURL).data(withMaxSize: 25 * 1024 * 1024, completion: { (data, error) -> Void in
            if let imageData = data, let image = UIImage(data: imageData){
                complete(image)
            }
        })
    }
}
