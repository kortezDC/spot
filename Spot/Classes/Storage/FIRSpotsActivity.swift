//  FIRSpotsActivity.swift
//  Spot

import Foundation
import Firebase

protocol FIRSpotsActivityProtocol {
    
   func updateCountersForSpot(spotUID:String, child: FIRChild, success : @escaping (Bool)->())
}

extension FIRSpotsActivityProtocol {
    
    /// Update spot model counters previews | likes (likes functionality for future)
    ///
    /// - Parameters:
    ///   - spotUID: spot what need update
    ///   - child: firebase model name
    ///   - success: block success
    func updateCountersForSpot(spotUID:String, child: FIRChild, success : @escaping (Bool)->()) {
        
        let firebaseRef = RootRouter.shared.firebaseRef.child(child.rawValue)
        
        guard let userUID = FIRAuth.auth()?.currentUser?.uid else {
            success(false)
            return
        }
        
        firebaseRef.child(spotUID).child(userUID).setValue(true) { (error, ref) in
            if error != nil {
                print("[Debug info] - \(error?.localizedDescription)")
                success(false)
            }else{
                
                firebaseRef.child(spotUID).observe(.value, with: { (snapshot) in
                    
                    let count = snapshot.children.allObjects.count                    
                    let params = [child.getParamName:count]
                    self.updateSpotCounters(spotUID: spotUID, params: params, success: { (result) in
                        success(result)
                    })
                })
            }
        }
    }
    
    func updateSpotCounters(spotUID : String, params:[String:Any], success : @escaping (Bool)->()){
        
        let firebaseRef = RootRouter.shared.firebaseRef.child(FIRChild.SpotsModels.rawValue)
        firebaseRef.child(spotUID).updateChildValues(params) { (error, reference) in
            if error != nil {
                print("[Debug info] - \(error?.localizedDescription)")
                success(false)
            }else{
                success(true)
            }
        }
    }
}
