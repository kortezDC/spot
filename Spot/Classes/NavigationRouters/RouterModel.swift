//  RouterModel.swift
//  Spot

import UIKit

struct RouterModel: Router, StoryboardsRouter {
    
    weak var presenter: UINavigationController?
    
     var login: LoginRouterModel!
     var home : HomeRouterModel!
     var explore: ExploreRouterModel!
     var createPhoto: TakePhotoRouterModel!
     var newSpot: NewSpotRouterModel!
     var settings: SettingsRouterModel!
    
    init() {}
    
    init!(presenter: UINavigationController?) {
        
        guard let _presenter = presenter else {
            return nil
         }
        
        self.presenter = _presenter
        login = LoginRouterModel(with: _presenter)
        home = HomeRouterModel(with: _presenter)
        explore = ExploreRouterModel(with: _presenter)
        createPhoto = TakePhotoRouterModel(with: _presenter)
        newSpot = NewSpotRouterModel(with: _presenter)
        settings = SettingsRouterModel(with: _presenter)
    }
}

