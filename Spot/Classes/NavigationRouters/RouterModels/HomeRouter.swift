//  HomeRouter.swift
//  Spot

import Foundation
import UIKit

struct HomeRouterModel: Router, StoryboardsRouter {
    
    weak var presenter: UINavigationController?
 
    init?(defaultPresenter: UINavigationController? = UINavigationController()) {
    
        presenter = defaultPresenter
        setRootViewController()
    }
    
    init!(with newPresenter: UINavigationController?) {
        
        guard let _presenter = newPresenter else {
            return nil
        }
        presenter = _presenter
    }
    
    func setRootViewController() {
        if let rootController = boardHome().instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
            presenter?.viewControllers = [rootController]
        }
    }
    
    func pushProccedPhoto(animation: Bool = true, image:ImageModel) {
        
        if let singlePhotoController = boardHome().instantiateViewController(withIdentifier: "SinglePhotoViewController") as? SinglePhotoViewController {
            singlePhotoController.viewModel = SinglePhotoViewModel(imageModel: image)
            push(singlePhotoController)
        }
    }
}
