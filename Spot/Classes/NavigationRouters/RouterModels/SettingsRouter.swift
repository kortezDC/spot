//  SettingsRouter.swift
//  Spot

import Foundation
import UIKit

struct SettingsRouterModel: Router, StoryboardsRouter {
    
    weak var presenter: UINavigationController?
    
    init?(defaultPresenter: UINavigationController? = UINavigationController()) {
        
        presenter = defaultPresenter
        setRootViewController()
    }
    
    init!(with newPresenter: UINavigationController?) {
        
        guard let _presenter = newPresenter else {
            return nil
        }
        presenter = _presenter
    }
    
    func setRootViewController() {
        
        if let rootController = boardSettings().instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController {
            presenter?.viewControllers = [rootController]
        }
    }
    
    func pushExploreSpot(animation: Bool = true, spot : SpotModel) {
        
        if let exploreSpotController = boardExplore().instantiateViewController(withIdentifier: "ExploreSelectedSpotViewController") as? ExploreSelectedSpotViewController {
            exploreSpotController.viewModel = ExploreSpotViewModel(withSpot: spot)
            presenter?.isNavigationBarHidden = false
            push(exploreSpotController)
        }
    }
}
