//  ExploreRouter.swift
//  Spot

import Foundation
import UIKit

struct ExploreRouterModel: Router, StoryboardsRouter {
    
    weak var presenter: UINavigationController?
    
    init?(defaultPresenter: UINavigationController? = UINavigationController()) {
        
        presenter = defaultPresenter
        setRootViewController()
    }
    
    init!(with newPresenter: UINavigationController?) {
        
        guard let _presenter = newPresenter else {
            return nil
        }
        presenter = _presenter
    }
    
    func setRootViewController() {
        if let rootController = boardExplore().instantiateViewController(withIdentifier: "ExploreViewController") as? ExploreViewController {
            presenter?.viewControllers = [rootController]
        }
    }
    
    func pushExploreSpot(animation: Bool = true, spot : SpotModel) {
        
        if let exploreSpotController = boardExplore().instantiateViewController(withIdentifier: "ExploreSelectedSpotViewController") as? ExploreSelectedSpotViewController {
            exploreSpotController.viewModel = ExploreSpotViewModel(withSpot: spot)
            presenter?.isNavigationBarHidden = false
            push(exploreSpotController)
        }
    }
    
    func pushProccedPhoto(animation: Bool = true, image : ImageModel) {
        
        if let singlePhotoController = boardHome().instantiateViewController(withIdentifier: "SinglePhotoViewController") as? SinglePhotoViewController {
            
            singlePhotoController.viewModel = SinglePhotoViewModel(imageModel: image)
            presenter?.isNavigationBarHidden = false
            push(singlePhotoController)
        }
    }
}
