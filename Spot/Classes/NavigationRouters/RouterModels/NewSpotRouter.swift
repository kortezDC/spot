//  NewSpotRouter.swift
//  Spot

import Foundation
import UIKit

struct NewSpotRouterModel: Router, StoryboardsRouter {
    
    weak var presenter: UINavigationController?
    
    init?(defaultPresenter: UINavigationController? = UINavigationController()) {
        
        presenter = defaultPresenter
        setRootViewController()
    }
    
    init!(with newPresenter: UINavigationController?) {
        
        guard let _presenter = newPresenter else {
            return nil
        }
        presenter = _presenter
    }
    
    func setRootViewController() {
        
        if let rootController = boardNewSpot().instantiateViewController(withIdentifier: "NewSpotViewController") as? NewSpotViewController {
            presenter?.viewControllers = [rootController]
        }
    }
}
