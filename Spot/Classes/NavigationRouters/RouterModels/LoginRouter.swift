//  LoginRouter.swift
//  Spot

import Foundation
import UIKit

struct LoginRouterModel: Router, StoryboardsRouter {
    
    weak var presenter: UINavigationController?
    
    init() {}
    
    init!(with newPresenter: UINavigationController?) {
        
        guard let _presenter = newPresenter else {
            return nil
        }
        presenter = _presenter
        presenter?.isNavigationBarHidden = true
    }
    
    func presentAuthRegistrationController(_ animation: Bool = true) {
        if let loginController = boardLogin().instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            push(loginController, animated: animation)
        }
    }
    
    func presentTabController(_ animation: Bool = true) {
        let tabBarController = CustomTabBarViewController()
        RootRouter.shared.window?.rootViewController = tabBarController
    }
}
