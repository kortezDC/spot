//
//  AuthRouterModel.swift
//  Mojo
//
//  Created by mac-Kyiv on 22.09.16.
//  Copyright © 2016 mac-Kyiv. All rights reserved.
//

import UIKit

struct AuthRouterModel: Router, StoryboardsRouter {
    
    weak var presenter: UINavigationController?
    
    init() {}
    
    init!(presenter: UINavigationController?) {
        
        guard let _presenter = presenter else {
            return nil
        }
        
        self.presenter = _presenter
    }
    
    func presentAuthRegistrationController(_ animation: Bool = true) {
        if let _presentController = storyAuthentication().instantiateViewController(withIdentifier: "AuthRegistrationController") as? AuthRegistrationController {
          
            let navController = UINavigationController(rootViewController: _presentController)
            
            present(navController, animated: animation)
        }
    }
    
    func pushAuthSignUpController() {
        if let _pushController = storyAuthentication().instantiateViewController(withIdentifier: "AuthSignUpController") as? AuthSignUpController {
            push(_pushController)
        }
    }
    
    func pushAuthLogInController() {
        if let _pushController = storyAuthentication().instantiateViewController(withIdentifier: "AuthLogInController") as? AuthLogInController {
            push(_pushController)
        }
    }
    
    func pushCreateAccountController() {
        if let _pushController = storyAuthentication().instantiateViewController(withIdentifier: "CreateAccountController") as? CreateAccountController {
            push(_pushController)
        }
    }
    
    func pushLogInAccountController() {
        if let _pushController = storyAuthentication().instantiateViewController(withIdentifier: "LogInAccountController") as? LogInAccountController {
            push(_pushController)
        }
    }
    
    func pushResetPasswordController() {
        if let _pushController = storyAuthentication().instantiateViewController(withIdentifier: "ResetPasswordController") as? ResetPasswordController {
            push(_pushController)
        }
    }
    
    func pushResetPasswordConfirmController() {
        if let _pushController = storyAuthentication().instantiateViewController(withIdentifier: "ResetPasswordConfirmController") as? ResetPasswordConfirmController {
            push(_pushController)
        }
    }
    
    
}
