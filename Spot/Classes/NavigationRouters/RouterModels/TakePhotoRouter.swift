//  CreatePhotoRouter.swift
//  Spot

import Foundation
import UIKit

struct TakePhotoRouterModel: Router, StoryboardsRouter {
    
    weak var presenter: UINavigationController?
    
    init?() {
        if let rootController = boardCreatePhoto().instantiateViewController(withIdentifier:
            "CreatePhotoViewController") as? CreatePhotoViewController {
            let defaultPresenter = UINavigationController(rootViewController: rootController)
            presenter = defaultPresenter
        }
     }
    
    init!(with newPresenter: UINavigationController?) {
        
        guard let _presenter = newPresenter else {
            return nil
        }
        presenter = _presenter
    }
    
    func startTakePhoto(){
        _ = presenter?.popToRootViewController(animated: false)
        presentImagePicker()
    }
    
    func presentImagePicker(animation : Bool = false) {
        
        let imagePicker = UIImagePickerController()
        
        guard let pickerDelegate = presenter?.viewControllers.first as? (UIImagePickerControllerDelegate & UINavigationControllerDelegate)? else {
            print(#function, "Debug info - pickerDelegate not set")
            return
        }
        
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        imagePicker.delegate = pickerDelegate
        presenter?.present(imagePicker, animated: animation, completion: nil)
    }
    
    func pushProccedPhoto(animation: Bool = false, image : UIImage?) {
        if let proccedPhotoController = boardCreatePhoto().instantiateViewController(withIdentifier: "ProccedPhotoViewController") as? ProccedPhotoViewController {
            proccedPhotoController.viewModel = ProccedPhotoViewModel(withImage: image!)
            presenter?.isNavigationBarHidden = false
            push(proccedPhotoController)
        }
    }
}
