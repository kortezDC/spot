//  StoryboardsRouter.swift
//  Spot

import UIKit

enum StoryboardsRouterList: String {
    case login = "Login"
    case home = "Home"
    case createPhoto = "TakePhoto"
    case explore = "Explore"
    case newSpot = "NewSpot"
    case settings = "Settings"
}

protocol StoryboardsRouter {
    func boardLogin() -> UIStoryboard
    func boardHome() -> UIStoryboard
    func boardExplore() -> UIStoryboard
    func boardCreatePhoto() -> UIStoryboard
    func boardNewSpot() -> UIStoryboard
    func boardSettings() -> UIStoryboard

}

extension StoryboardsRouter {
    
    func boardLogin() -> UIStoryboard {
        return UIStoryboard(name: StoryboardsRouterList.login.rawValue, bundle: nil)
    }
    
    func boardHome() -> UIStoryboard {
        return UIStoryboard(name: StoryboardsRouterList.home.rawValue, bundle: nil)
    }

    func boardExplore() -> UIStoryboard {
        return UIStoryboard(name: StoryboardsRouterList.explore.rawValue, bundle: nil)
    }

    func boardCreatePhoto() -> UIStoryboard {
        return UIStoryboard(name: StoryboardsRouterList.createPhoto.rawValue, bundle: nil)
    }
    
    func boardNewSpot() -> UIStoryboard {
        return UIStoryboard(name: StoryboardsRouterList.newSpot.rawValue, bundle: nil)
    }
    
    func boardSettings() -> UIStoryboard {
        return UIStoryboard(name: StoryboardsRouterList.settings.rawValue, bundle: nil)
    }
}
