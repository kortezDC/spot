//  NewSpotViewController.swift
//  Spot

import Foundation
import UIKit
import GoogleMaps

class NewSpotViewController : BaseController {
    
    @IBOutlet fileprivate var mapView : GMSMapView!
    @IBOutlet var titleTextField : UITextField!
    @IBOutlet var descriptionTextField : UITextField!

    internal var viewModel = NewSpotViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        configureMap()
        title = "Add Spot"
        /// Observe keyboard actions
        NotificationCenter.default.addObserver(self, selector: #selector(self.showKeyboardUpView(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideKeyboardUpView(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        viewModel.controllerRouter = router
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /// Observe action when location updated
        NotificationCenter.default.addObserver(self, selector: #selector(updateCurrentLocation(_:)), name: NSNotification.Name(rawValue: "LocationUpdated"), object: nil)

        /// Fetch current location
        LocationManager.shared.getCurrentLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func updateCurrentLocation(_:AnyObject){
        /// get current place
        self.descriptionTextField.text = LocationManager.shared.currentPlace
        
        let location = LocationManager.shared.currentLocation
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        self.mapView?.animate(to: camera)
    }
    
    func configureMap(){
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: DefaultGeo.latitude.rawValue, longitude: DefaultGeo.longitude.rawValue, zoom: 12.05)
        mapView.camera = camera
        mapView.delegate = self
    }
    
    @IBAction func addSpotAction(_ sender: UIButton?){
        viewModel.proccedSpot(title: titleTextField?.text, description: descriptionTextField?.text)
    }
}

/// GMSMapView delegate methods
/// Tap by map action
extension NewSpotViewController : GMSMapViewDelegate {
    
     func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        mapView.clear()
        let marker = GMSMarker(position: coordinate)
        marker.title = "New Spot"
        marker.isFlat = true
        marker.map = mapView
        
        viewModel.setLocation(location: coordinate)
    }
}

extension NewSpotViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == titleTextField{
            descriptionTextField.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
}
