//  NewSpotViewModel.swift
//  Spot

import Foundation
import GeoFire

class NewSpotViewModel : ViewModel, FIRAddSpotProtocol {

    internal var newSpot : SpotModel? = nil
    internal var controllerRouter : RouterModel? = nil
    
    internal var firebaseRef = RootRouter.shared.firebaseRef.child(FIRChild.SpotsModels.rawValue)
    internal var geoFire : GeoFire? = GeoFire(firebaseRef: RootRouter.shared.firebaseRef.child(FIRChild.GeofireSpots.rawValue))
    internal var queryFilterLimit : UInt = 0
    
    func setLocation(location : CLLocationCoordinate2D){
        
        if newSpot == nil {
            newSpot = SpotModel()
        }
        
        newSpot?.latitude = location.latitude
        newSpot?.longitude = location.longitude
    }
    
    func alertAction(alertAction : UIAlertAction) {
       controllerRouter!.explore.pushExploreSpot(spot: newSpot!) 
    }
}
