//  ExploreViewController.swift
//  Spot

import Foundation
import UIKit
import GoogleMaps

class ExploreViewController : BaseController {
    
    @IBOutlet internal var mapView : GMSMapView!
    @IBOutlet fileprivate var tableView : UITableView!
    @IBOutlet fileprivate var spotsCountLabel : UILabel!

    
    fileprivate let reuseIdentifier = "ExploreTableCell"
    internal var viewModel = ExploreViewModel()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        viewModel.delegate = self
        configureTableView()
        title = "Explore"
    }
    
    func configureTableView() {
        tableView.register(ExploreTableCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /// Observe action when location updated
        navigationController?.isNavigationBarHidden = false
        viewModel.getSpotsByFilter(childParams: .previews)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
}

extension ExploreViewController : ViewModelDelegate {
    
    func viewModelDidStartUpdate() {
        // TODO
    }
    
    func viewModelDidEndUpdate() {
        // TODO
        viewModel.sortContainer()
        viewModel.fillMapsView(map: mapView)
        spotsCountLabel.text = String(viewModel.container.count)
        UIApplication.hideActivity()
        tableView.reloadData()
    }
}

// MARK:  UITableViewDataSource
extension ExploreViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.container.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ExploreTableCell
        
        let spot = viewModel.getSpotByIndex(index: indexPath.row)
        cell.spotTitleLabel.text = spot.title
        cell.eyesCounterLabel.text = String(spot.previews)
        cell.likesCounterLabel.text = String(spot.likes)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedSpot = viewModel.getSpotByIndex(index: indexPath.row)
        // selectedSpot
        viewModel.updateCountersForSpot(spotUID: selectedSpot.spotUID, child: FIRChild.PreviewModels) { (result) in  }
        router.explore.pushExploreSpot(spot: selectedSpot)
    }
}
