//  ExploreTableCell.swift
//  Spot

import Foundation
import UIKit

class ExploreTableCell : UITableViewCell {
    
    @IBOutlet var spotTitleLabel : UILabel!
    @IBOutlet var eyesCounterLabel : UILabel!
    @IBOutlet var likesCounterLabel : UILabel!
}
