//  ExploreViewModel.swift
//  Spot

import Foundation
import GeoFire

class ExploreViewModel<T:SpotModel> : ViewModel, FIRSpotsActivityProtocol, FIRSpotsLocationsProtocol {
    
    internal var firebaseRef = RootRouter.shared.firebaseRef.child(FIRChild.SpotsModels.rawValue)
    internal var geoFire : GeoFire? = nil
    internal var queryFilterLimit : UInt = 10

    var selectedFilter = ChildParams.previews
    
    func getSpotByIndex(index:Int) -> T{
        
        guard  container.indices.contains(index) else {
            return T()
        }
        return container[index] as! T
    }
    
    /// Sort
    override func sortContainer(){
        
        switch selectedFilter {
        case .previews:
            container.sort(by: { ($0 as! T).previews > ($1 as! T).previews })
        default:
            container.sort(by: { ($0 as! T).likes > ($1 as! T).likes })
        }
    }
}
