//  ExploreSpotCollectionCell.swift
//  Spot

import Foundation
import UIKit

class ExploreImageCollectionCell : UICollectionViewCell{
    
    @IBOutlet var spotImageView : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()        
    }
}
