//  ExploreSelectedSpot.swift
//  Spot

import Foundation
import UIKit
import GoogleMaps

class ExploreSelectedSpotViewController : BaseController {
    
    @IBOutlet internal var mapView : GMSMapView!
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var descriptionLabel : UILabel!
    @IBOutlet var ownerLabel: UILabel!
    @IBOutlet weak var imagesCounterLabel: UILabel!
    
    internal var viewModel : ExploreSpotViewModel? = nil
    fileprivate let reuseIdentifier = "ExploreImageCollectionCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 1.0, left: 16.0, bottom: 2.0, right: 16.0)
    fileprivate let itemsPerRow: CGFloat = 3

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = viewModel?.getSpotLocationName
        viewModel?.delegate = self
        collectionView.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        configureMap()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    
    func configureMap(){
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: viewModel!.selectedSpot!.latitude, longitude: viewModel!.selectedSpot!.longitude, zoom: 12.05)
        mapView.camera = camera
        // Creates a marker
        let spotMarker = GMSMarker(position: viewModel!.selectedSpot!.spotCoordinates)
        spotMarker.appearAnimation = GMSMarkerAnimation.none
        spotMarker.map = mapView
    }
}

extension ExploreSelectedSpotViewController : ViewModelDelegate {
    
    func viewModelDidStartUpdate() {
        ownerLabel.text = ownerLabel.text! + " " + viewModel!.getOwnerSpotData
        descriptionLabel.text = viewModel!.getSpotDescription
    }
    
    func viewModelDidEndUpdate() {
        UIApplication.hideActivity()
        imagesCounterLabel.text = String(viewModel!.container.count)
        collectionView.reloadData()
    }
}

extension ExploreSelectedSpotViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return viewModel!.container.count
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ExploreImageCollectionCell
        cell.spotImageView.kf.indicatorType = .activity

        let imageModel = viewModel!.getImageByIndex(index: indexPath.row)
        cell.spotImageView.kf.setImage(with: URL(string:imageModel.imageURL))

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedImage = viewModel!.getImageByIndex(index: indexPath.row)
        router.explore.pushProccedPhoto(image: selectedImage)
    }
}

extension ExploreSelectedSpotViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
