//  ExploreSpotViewModel.swift
//  Spot

import Foundation
import UIKit
import GeoFire

class ExploreSpotViewModel<T:ImageModel> : ViewModel, FIRImagesLocationsProtocol {

    /// protocol declaration of variables
    internal var queryFilterLimit: UInt = 10
    internal var geoFire: GeoFire? = GeoFire(firebaseRef: RootRouter.shared.firebaseRef.child(FIRChild.GeofireImages.rawValue))
    internal var firebaseRef = RootRouter.shared.firebaseRef.child(FIRChild.ImagesModels.rawValue)

    /// model variables
    internal var selectedSpot : SpotModel? = nil

    init(withSpot spot: SpotModel) {
        super.init()
        selectedSpot = spot
        /// get images in area
        getImagesByLocation(spot: spot)
    }
    
    var getSpotLocationName : String {
        get {
            guard let entity = selectedSpot else{
                return ""
            }
            return entity.title
        }
    }
    
    var getOwnerSpotData: String {
        get {
            guard let entity = selectedSpot else{
                return ""
            }
            return entity.ownerName
        }
    }
    
    var getSpotDescription : String {
        get {
            guard let entity = selectedSpot else{
                return ""
            }
            return entity.spotDescription
        }
    }
    
    func getImageByIndex(index:Int) -> T{
        
        guard  container.indices.contains(index) else {
            return T()
        }
        return container[index] as! T
    }
}
