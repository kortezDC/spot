//  ViewController.swift
//  Spot

import UIKit

class LoginViewController: BaseController {

    @IBOutlet var emailTextField : UITextField!
    @IBOutlet var passwordTextField : UITextField!
    
    internal var viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        viewModel.delegate = self
    }
    
    @IBAction func loginAction(_ sender: UIButton){
        
        viewModel.loginByEmailAction(email: emailTextField.text!, password: passwordTextField.text!)
    }
}

extension LoginViewController : ViewModelDelegate {
    
    func viewModelDidEndUpdate() {
        router.login.presentTabController()
    }
    func viewModelDidStartUpdate() {
        /// TODO
    }
}
