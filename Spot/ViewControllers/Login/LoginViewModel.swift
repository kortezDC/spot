//  LoginViewModel.swift
//  Spot

import Foundation
import FirebaseAuth
import UIKit

/// Represent logic of email login and email verification functionality
class LoginViewModel : ViewModel, FIRUserManagerProtocol {
    
    /// Firbase API call for user login functionality
    /// - Parameters:
    ///   - email: user-entered email
    ///   - password: user-entered password
    func loginByEmailAction(email:String, password:String) {
        
        /// Login user
        FIRAuth.auth()?.signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                UIApplication.showAlertMessage(message: error!.localizedDescription)
            } else {
                /// update navigation
                guard let loginedUser = user  else {
                    return
                }
                
                /// Example
                let userModel = UserModel()
                userModel.uid = loginedUser.uid
                userModel.firstname = "test"
                userModel.lastname = "test"
                userModel.email = loginedUser.email!
                
                if let user = user {
                    let changeRequest = user.profileChangeRequest()
                    
                    changeRequest.displayName = userModel.getFullName
                    
                    changeRequest.commitChanges(completion: { (error) in
                        if error == nil {
                            self.createUserModelFIB(user: userModel)
                            self.delegate?.viewModelDidEndUpdate()
                        }else{
                            print("[Debuf info - \(error?.localizedDescription)]")
                        }
                    })
                }
            }
        }
    }
}
