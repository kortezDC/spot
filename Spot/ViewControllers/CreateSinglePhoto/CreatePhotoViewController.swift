//  CreatePhotoViewController.swift
//  Spot

import Foundation
import UIKit
import Photos
import MobileCoreServices

class CreatePhotoViewController : BaseController {
    
    var resultImage : UIImage? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        LocationManager.shared.getCurrentPlace()
    }
}

extension CreatePhotoViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: false) {
            self.router.createPhoto.presenter?.tabBarController?.selectedIndex = 0
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            resultImage = image
        }
        
        picker.dismiss(animated: false, completion: {
            self.router.createPhoto.pushProccedPhoto(image: self.resultImage)
        })
    }
}
