//  ProccedPhotoViewModel.swift
//  Spot

import Foundation
import UIKit
import Firebase
import CoreLocation

class ProccedPhotoViewModel: ViewModel, FIRGeofireProtocol {
    
    let newImageRef = RootRouter.shared.firebaseRef.child(FIRChild.ImagesModels.rawValue).childByAutoId()

    private var spotImage : UIImage? = nil
    private var imageModel = ImageModel()
    let storageSpot = FIRStorageSpotPhoto()
    internal var image : UIImage? {
        return spotImage
    }
    
    var getImageTitle : String {
        get {
            if imageModel.title.characters.count < 1 {
                imageModel.title = LocationManager.shared.currentCity
            }
            return imageModel.title
        }set{
            imageModel.title = newValue
        }
    }
    
    init(withImage takenImage:UIImage) {
        super.init()
        
        spotImage = takenImage
        
        if let ownerName = currentUser?.displayName {
            imageModel.ownerName = ownerName
        }else {
            imageModel.ownerName = (currentUser?.email)!
        }
    }
    
    /// Upload image with current location
    func addImageWithLocation(){
        
        UIApplication.showHUD()
        /// Upload image to Firebase
        self.storageSpot.uploadPhoto(spotImage!, complate: { (photoURL, value) in
            /// Create ImageModel
            self.createImageModel(imageUID: value!, imageURL: photoURL!.absoluteString, success: { (imageSpotRef) in
            })
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    /// Create firebase ImageSpot model
    ///
    /// - Parameters:
    ///   - imageUID: UID uploaded image
    ///   - imageURL: URL uploaded image
    ///   - success: async return created image spot model reference
    func createImageModel(imageUID:String, imageURL: String, success:@escaping (String)->()) {
        
        imageModel.imageUID = imageUID
        imageModel.imageURL = imageURL
        imageModel.title = getImageTitle
        imageModel.latitude = (LocationManager.shared.currentLocation?.coordinate.latitude)!
        imageModel.longitude = (LocationManager.shared.currentLocation?.coordinate.longitude)!
        imageModel.ownerUID = (currentUser?.uid)!
        
        if let ownerName = currentUser?.displayName {
            imageModel.ownerName = ownerName
        }else {
            imageModel.ownerName = (currentUser?.email)!
        }
        
        imageModel.timestamp = Date().timeIntervalSince1970
        
        /// image locations
        let imageLocation = CLLocation(latitude: imageModel.latitude, longitude: imageModel.longitude)
        
        /// Firebase add new image model
        newImageRef.setValue(imageModel.toJSON()) { (error, reference) in

            UIApplication.hideActivity()
            if error == nil {
                self.addGeofire(child: .GeofireImages, nodeKey: reference.key, locations: imageLocation)
                UIApplication.showAlertMessageWithAction(message: "Image successfully added", handler: self.alertAction)
            }else{
                print("[Debug info] - \(error?.localizedDescription)")
            }
        }
    }
    
    func alertAction(alertAction : UIAlertAction) {
        UIApplication.topViewController()?.tabBarController?.selectedIndex = 0
    }
}
