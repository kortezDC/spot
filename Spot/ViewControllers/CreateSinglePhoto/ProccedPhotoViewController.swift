//  ProccedPhotoViewController.swift
//  Spot

import Foundation
import UIKit

class ProccedPhotoViewController : BaseController {
    
    @IBOutlet var imageView : UIImageView!
    @IBOutlet weak var spotNameButton: UIButton!
    @IBOutlet weak var titleTextField : UITextField!
    
    internal var viewModel : ProccedPhotoViewModel? = nil
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showKeyboardUpView(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideKeyboardUpView(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        if (self.isMovingFromParentViewController){
            router.createPhoto.startTakePhoto()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imageView.image = viewModel?.image
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        viewModel?.getImageTitle = titleTextField.text!
        viewModel?.addImageWithLocation()
    }
}

extension ProccedPhotoViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
