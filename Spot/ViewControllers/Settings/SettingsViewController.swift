//
//  SettingsViewController.swift
//  Spot

import Foundation
import UIKit
import GoogleMaps

class SettingsViewController : BaseController {
    
    @IBOutlet internal var mapView : GMSMapView!
    @IBOutlet var userEmailLabel : UILabel!
    @IBOutlet weak var profileInfoView: UIView!
    @IBOutlet weak var spotsView: UIView!
    @IBOutlet weak var segmentControll: UISegmentedControl!
    
    internal lazy var viewModel =  SettingsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Settings"
        userEmailLabel.text = viewModel.getUserEmail
        viewModel.delegate = self
        mapView.delegate = self
    }
    
    @IBAction func indexChanged(sender:UISegmentedControl) {
        
        profileInfoView.isHidden = true
        spotsView.isHidden = true
        
        switch sender.selectedSegmentIndex {
        case 0: /// profile
            profileInfoView.isHidden = false
        case 1: /// subscribers
            break
        case 2: /// spots
            spotsView.isHidden = false
            viewModel.getFIBUserSpots()
            
        default:
            break;
        }
    }
}

extension SettingsViewController : ViewModelDelegate {
    
    func viewModelDidStartUpdate() {
        /// TODO
    }
    
    func viewModelDidEndUpdate() {
        /// TODO
        switch segmentControll.selectedSegmentIndex {
        case 2:
            viewModel.fillMapsView(map: mapView)
        default:break
        }

    }
}

/// GMSMapView delegate methods
/// Tap by map action
extension SettingsViewController : GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        guard let spot = marker.userData as? SpotModel else {
            print("wrong spot")
            return
        }
        router.settings.pushExploreSpot(spot: spot)
    }
}
