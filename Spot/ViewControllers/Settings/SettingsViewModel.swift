//  SettingsViewModel.swift
//  Spot

import Foundation
import GeoFire

class SettingsViewModel<T:SpotModel> : ViewModel, FIRSpotsLocationsProtocol {
    
    internal var firebaseRef = RootRouter.shared.firebaseRef.child(FIRChild.SpotsModels.rawValue)
    internal var geoFire : GeoFire? = nil
    internal var queryFilterLimit : UInt = 0
    
    var getUserEmail : String {
        return (UserManager.shared.getLoggedInUser?.email)!
    }
}
