//  CustomTabBarViewController.swift
//  Spot

import Foundation
import UIKit

class CustomTabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        delegate = self
        
        /// Home photo tab
        let homeCoordinator = HomeRouterModel()
        /// Explore tab
        let exploreCoordinator = ExploreRouterModel()
        /// Take photo tab
        let takePhotoCoordinator = TakePhotoRouterModel()
        /// New spot tab
        let newSpotCoordinator = NewSpotRouterModel()
        /// Settings tab
        let settingsCoordinator = SettingsRouterModel()
        
        /// create tabs icons
        let homeIcon = UITabBarItem(title: "Home", image: UIImage(named: "home_icon.png"), selectedImage: UIImage(named: "home_icon.png"))
        let exploreIcon = UITabBarItem(title: "Explore", image: UIImage(named: "explore_icon.png"), selectedImage: UIImage(named: "explore_icon.png"))
        let takePhotoIcon = UITabBarItem(title: "Camera", image: UIImage(named: "camera_icon.png"), selectedImage: UIImage(named: "camera_icon.png"))
        let newSpotIcon = UITabBarItem(title: "Add Spot", image: UIImage(named: "addspot_icon.png"), selectedImage: UIImage(named: "addspot_icon.png"))
        let settingsIcon = UITabBarItem(title: "Settings", image: UIImage(named: "settings_icon.png"), selectedImage: UIImage(named: "settings_icon.png"))
        
        homeCoordinator!.presenter!.tabBarItem = homeIcon
        exploreCoordinator!.presenter!.tabBarItem = exploreIcon
        takePhotoCoordinator!.presenter!.tabBarItem = takePhotoIcon
        newSpotCoordinator!.presenter!.tabBarItem = newSpotIcon
        settingsCoordinator!.presenter!.tabBarItem = settingsIcon
        
        /// array of the root view controllers displayed by the tab bar interface
        let controllers = [homeCoordinator!.presenter!, exploreCoordinator!.presenter!, takePhotoCoordinator!.presenter!, newSpotCoordinator!.presenter!, settingsCoordinator!.presenter!]
        self.viewControllers = controllers
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //back from openURL method
    func applicationWillEnterForeground() {
        self.selectedIndex = 0
    }
    
    func isAccessLocationAllow() -> Bool {
        
        /// check if user apply access to location manager
        let locStatus = LocationManager.shared.getAuthorizationStatus
        if  locStatus == .authorizedWhenInUse ||  locStatus == .authorizedAlways{
            return true
        }else{
            self.selectedIndex = 0
            LocationManager.shared.checklLocationAuthorizationStatus(status: LocationManager.shared.getAuthorizationStatus)
            return false
        }
    }
}

//Delegate methods
extension CustomTabBarViewController: UITabBarControllerDelegate{
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true;
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        switch tabBarController.viewControllers!.index(of: viewController)! as Int {
        case 2:
            if isAccessLocationAllow() {
                guard let controller = (viewController as? UINavigationController)?.viewControllers.last else{
                    return
                }
                (controller as? BaseController)?.router.createPhoto.startTakePhoto()
            }
        case 3:
            if !isAccessLocationAllow(){
                return
            }
        default:
            break
        }
    }
}
