//  SinglePhotoViewController.swift
//  Spot

import Foundation
import UIKit

class SinglePhotoViewController : BaseController {
    
    internal var viewModel : SinglePhotoViewModel? = nil
    @IBOutlet var imageView : UIImageView!
    @IBOutlet var creatorLabel : UILabel!
    @IBOutlet weak var eyeCounterLabel: UILabel!
    @IBOutlet weak var likesCounterLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
    }
}

extension SinglePhotoViewController : ViewModelDelegate{
    
    func viewModelDidStartUpdate() {
        UIApplication.hideActivity()
        self.title = viewModel?.getImageTitle
        creatorLabel!.text = creatorLabel.text! + viewModel!.getOwnerImageData
        imageView.kf.indicatorType = .activity
        let url = URL(string: viewModel!.getImageURL)
        imageView.kf.setImage(with: url)
        eyeCounterLabel.text = String(viewModel!.spotImage!.previews)
        likesCounterLabel.text = String(viewModel!.spotImage!.likes)
    }
    
    func viewModelDidEndUpdate() {
        // TODO
    }
}
