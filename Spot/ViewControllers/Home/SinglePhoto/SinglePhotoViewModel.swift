//  SinglePhotoViewModel.swift
//  Spot

import Foundation
import Firebase

class SinglePhotoViewModel : ViewModel {
    
    internal var spotImage : ImageModel? = nil
    var getImageTitle : String {
        get {
            guard let entity = spotImage else{
                return ""
            }
            return entity.title
        }
    }
    
    var getImageURL : String{
        get {
            guard let entity = spotImage else{
                return ""
            }
            return entity.imageURL
        }
    }
    
    var getOwnerImageData: String {
        get {
            guard let entity = spotImage else{
                return ""
            }
            return entity.ownerName + " " + String.formateDate(created_at: entity.timestamp)
        }
    }
    
    init(imageModel: ImageModel) {
        super.init()
        
         spotImage = imageModel
    }
}
