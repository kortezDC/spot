//  HomeCollectionCell.swift
//  Spot

import Foundation
import UIKit

class HomeCollectionCell : UICollectionViewCell {
    
    @IBOutlet var spotImageView : UIImageView!
    @IBOutlet var spotNameLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()        
    }
}
