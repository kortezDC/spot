//  HomeViewModel.swift
//  Spot

import Foundation
import GeoFire

class HomeViewModel <T:ImageModel> : ViewModel, FIRImagesLocationsProtocol {

    /// protocol declaration of variables
    internal var geoFire: GeoFire? = nil
    internal var queryFilterLimit : UInt = 0
    internal var firebaseRef = RootRouter.shared.firebaseRef.child(FIRChild.ImagesModels.rawValue)
    
    func getImageByIndex(index:Int) -> T{
        
        guard  container.indices.contains(index) else {
            return T()
        }
        return container[index] as! T
    }
}
