//  HomeViewController.swift
//  Spot

import Foundation
import UIKit
import Kingfisher

class HomeViewController : BaseController {
    
    internal let viewModel = HomeViewModel()
    @IBOutlet var collectionView : UICollectionView!
    
    fileprivate let reuseIdentifier = "HomeCollectionCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 1.0, left: 10.0, bottom: 2.0, right: 10.0)
    fileprivate let itemsPerRow: CGFloat = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self
        collectionView.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        
        viewModel.getFIBUserCreatedImages()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }    
}

extension HomeViewController : ViewModelDelegate{
    
    func viewModelDidStartUpdate() {}
    
    func viewModelDidEndUpdate() {
        
        viewModel.sortContainer()
        UIApplication.hideActivity()
        collectionView.reloadData()
    }
}

extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return viewModel.container.count
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomeCollectionCell
        
        let imageModel = viewModel.getImageByIndex(index: indexPath.row)
        cell.spotImageView.kf.indicatorType = .activity
        if let url = URL(string: imageModel.imageURL) {
            let cache = ImageCache(name: "longer_cache")
            cache.maxDiskCacheSize = UInt(60 * 60 * 24 * 30)
            cell.spotImageView.kf.setImage(with: url)
        }else{
            cell.spotImageView.image = UIImage(named: "spot_placeholder")
        }
        cell.spotNameLabel.text = imageModel.title
        cell.spotImageView.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedImage = viewModel.getImageByIndex(index: indexPath.row)
        router.home.pushProccedPhoto(image: selectedImage)
    }
}

extension HomeViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem + 20)
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
